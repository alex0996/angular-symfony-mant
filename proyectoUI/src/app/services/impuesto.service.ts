import { Injectable } from '@angular/core';
import { Impuesto } from '../model/Impuesto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ImpuestoService {

  constructor(private http: HttpClient) { }
   
  getImpuestos (): Observable<Impuesto[]> {
    const url = 'http://127.0.0.1:8000/impuestos';
    return this.http.get<Impuesto[]>(url)
      .pipe(
        tap(heroes => this.log('Se han cargado los impuestos')),
        catchError(this.handleError('getImpuesto', []))
      );
  }


  getImpuesto (impuesto: Impuesto): Observable<Impuesto> {
    const url = `http://127.0.0.1:8000/impuestos/${impuesto.id_impuesto}`;
    return this.http.get<Impuesto>(url)
      .pipe(
        tap(_ => this.log('Se ha cargado el impuesto')),
        catchError(this.handleError('getImpuesto', impuesto))
      );
  }

   /** POST: add a new hero to the server */
   agregarImpuesto (impuesto: Impuesto): Observable<Impuesto> {
    const url = 'http://127.0.0.1:8000/agregarImpuesto';
    return this.http.post<Impuesto>(url, impuesto, httpOptions).pipe(
      tap((impuesto: Impuesto) => this.log(`se agrego el impuesto w/ id=${impuesto.id_impuesto}`)),
      catchError(this.handleError<Impuesto>('agregar impuesto'))
    );
  }

  editarImpuesto (impuesto: Impuesto): Observable<any> {
    const url = `http://127.0.0.1:8000/editarImpuesto/${impuesto.id_impuesto}`;
    return this.http.put(url, impuesto, httpOptions).pipe(
      tap(_ => this.log(`impuesto actualizado id=${impuesto.id_impuesto}`)),
      catchError(this.handleError<any>('actualizar impuesto'))
    );
  }

  eliminarImpuesto (impuesto: Impuesto): Observable<Impuesto> {
    const url = `http://127.0.0.1:8000/eliminarImpuesto/${impuesto.id_impuesto}`;

    return this.http.get<Impuesto>(url).pipe(
      tap(_ => this.log(`impuesto eliminado id=${impuesto.id_impuesto}`)),
      catchError(this.handleError<Impuesto>('eliminarimpuesto'))
    );
  }


   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a message */
  private log(message: string) {
    console.log(message);
  }



}
