import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { AppComponent } from './app.component';
import { ImpuestoComponent } from './components/impuesto/impuesto.component';
import { ImpuestoService } from './services/impuesto.service';

@NgModule({
  declarations: [
    AppComponent,
    ImpuestoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ImpuestoService],
  bootstrap: [AppComponent]
})

export class AppModule { }
