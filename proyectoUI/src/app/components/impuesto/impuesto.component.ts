import { Component, OnInit, ViewChild } from '@angular/core';
import { Impuesto } from '../../model/Impuesto';
import { ImpuestoService } from '../../services/impuesto.service';

@Component({
  selector: 'app-impuesto',
  templateUrl: './impuesto.component.html',
  styleUrls: ['./impuesto.component.css']
})
export class ImpuestoComponent implements OnInit {
  //impuestos
  impuestos: Impuesto[];
  impuesto = new Impuesto();
  //inputs
  @ViewChild("id_impuesto") id_impuesto;
  @ViewChild("nombre_impuesto") nombre_impuesto;
  @ViewChild("monto") monto;
  @ViewChild("fecha_desde") fecha_desde;
  @ViewChild("fecha_hasta") fecha_hasta;
  //botones
  agregar: boolean;
  editar: boolean;
  
  constructor(private impuestoService: ImpuestoService) { }

  ngOnInit() {
    this.getImpuestos();
    this.agregar = true;
    this.editar = false;
  }

  getImpuestos(): void {
    this.impuestoService.getImpuestos()
    .subscribe(impuestos => this.impuestos = impuestos);

  }

  agregarImpuesto(id_impuesto: number, nombre_impuesto: string, monto:number, fecha_desde: string, fecha_hasta: string): void {
    this.impuestoService.agregarImpuesto({id_impuesto, nombre_impuesto, monto, fecha_desde, fecha_hasta } as Impuesto)
      .subscribe(impuestos => {
        this.impuestos.push(impuestos);
      });
      this.volver();
  }

  obtenerImpuesto(impuesto: Impuesto): void {
    this.impuestoService.getImpuesto(impuesto)
    .subscribe(impuesto => this.impuesto = impuesto);
    this.agregar = false;
    this.editar = true;
  }

  editarImpuesto(id_impuesto: number, nombre_impuesto: string, monto:number, fecha_desde: string, fecha_hasta: string): void {
    this.impuestoService.editarImpuesto({id_impuesto, nombre_impuesto, monto, fecha_desde, fecha_hasta } as Impuesto)
      .subscribe(response => this.getImpuestos());
    
    this.volver();
  }


  eliminarImpuesto(impuesto: Impuesto): void {
    this.impuestoService.eliminarImpuesto(impuesto)
    .subscribe(response => this.getImpuestos());
  }


  volver(): void {
    this.agregar = true;
    this.editar = false;
    this.impuesto = new Impuesto();
  }

}
