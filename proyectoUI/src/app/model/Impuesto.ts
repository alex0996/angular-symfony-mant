export class Impuesto {
    id_impuesto: number;
    nombre_impuesto: string;
    monto: number;
    fecha_desde: string; 
    fecha_hasta: string;

    constructor(){
      this.id_impuesto = 0;
      this.nombre_impuesto = "";
      this.monto = 0;
      this.fecha_desde = "";
      this.fecha_hasta = "";
     
  }

  }
  