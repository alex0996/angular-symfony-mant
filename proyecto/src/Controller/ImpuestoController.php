<?php
namespace App\Controller;

use App\Entity\Impuesto;
use App\Repository\ImpuestoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ImpuestoController extends ApiController
{
    /**
    * @Route("/impuestos")
    * @Method("GET")
    */
    public function index(ImpuestoRepository $impuestoRepository)
    {
        $impuestos = $impuestoRepository->transformAll();

        return $this->respond($impuestos);
    }

     /**
     * @Route("/impuestos/{id}")
     * @Method("GET")
     */
    public function show($id,ImpuestoRepository $impuestoRepository)
    {
        $impuesto = $impuestoRepository->find($id);

        if (! $impuesto) {
            return $this->respondNotFound();
        }

        $impuesto = $impuestoRepository->transform($impuesto);

        return $this->respond($impuesto);
    }

     /**
     * @Route("/eliminarImpuesto/{id}")
     * @Method("GET")
     */
    public function delete($id,ImpuestoRepository $impuestoRepository,EntityManagerInterface $em)
    {
        $impuesto = $impuestoRepository->find($id);

        if (! $impuesto) {
            return $this->respondNotFound();
        }

        $eliminado = $impuestoRepository->transform($impuesto);
        
        $em->remove($impuesto);
        $em->flush();
        
        return $this->respond($eliminado);
    }


    /**
    * @Route("/agregarImpuesto", name="agregarImpuesto", methods={"POST"})
    * @Method("POST")
    */
    public function create(Request $request, ImpuestoRepository $impuestoRepository, EntityManagerInterface $em)
    {
       
       
        if (! $request) {
            return $this->respondValidationError('Please provide a valid request!');
        }

        // persist the new object
        $impuesto = new Impuesto;
        $impuesto->setIdImpuesto($request->get('id_impuesto'));
        $impuesto->setNombreImpuesto($request->get('nombre_impuesto'));
        $impuesto->setMonto($request->get('monto'));
        $impuesto->setFechaDesde($request->get('fecha_desde'));
        $impuesto->setFechaHasta($request->get('fecha_hasta'));

        $em->persist($impuesto);
        $em->flush();

        return $this->respondCreated($impuestoRepository->transform($impuesto));
    }


    
    /**
    * @Route("/editarImpuesto/{id}", name="editarImpuesto", methods={"PUT"})
    * @Method("POST")
    */
    public function update($id, Request $request, ImpuestoRepository $impuestoRepository, EntityManagerInterface $em)
    {
       
        $impuesto = $impuestoRepository->find($id);

        if (! $impuesto) {
            return $this->respondNotFound();
        }
        $impuesto->setNombreImpuesto($request->get('nombre_impuesto'));
        $impuesto->setMonto($request->get('monto'));
        $impuesto->setFechaDesde($request->get('fecha_desde'));
        $impuesto->setFechaHasta($request->get('fecha_hasta'));

        $em->persist($impuesto);
        $em->flush();

        return $this->respondCreated($impuestoRepository->transform($impuesto));
    }


}