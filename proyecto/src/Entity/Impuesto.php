<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImpuestoRepository")
 */
class Impuesto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id_impuesto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_impuesto;

    /**
     * @ORM\Column(type="float")
     */
    private $monto;

    /**
     * @ORM\Column(type="string")
     */
    private $fecha_desde;

    /**
     * @ORM\Column(type="string")
     */
    private $fecha_hasta;


    public function getIdImpuesto(): ?int
    {
        return $this->id_impuesto;
    }

    public function setIdImpuesto(int $id_impuesto): self
    {
        $this->id_impuesto = $id_impuesto;

        return $this;
    }

    public function getNombreImpuesto(): ?string
    {
        return $this->nombre_impuesto;
    }

    public function setNombreImpuesto(string $nombre_impuesto): self
    {
        $this->nombre_impuesto = $nombre_impuesto;

        return $this;
    }

    public function getMonto(): ?float
    {
        return $this->monto;
    }

    public function setMonto(float $monto): self
    {
        $this->monto = $monto;

        return $this;
    }

    public function getFechaDesde(): ?string
    {
        return $this->fecha_desde;
    }

    public function setFechaDesde(string $fecha_desde): self
    {
        $this->fecha_desde = $fecha_desde;

        return $this;
    }

    public function getFechaHasta(): ?string
    {
        return $this->fecha_hasta;
    }

    public function setFechaHasta(string $fecha_hasta): self
    {
        $this->fecha_hasta = $fecha_hasta;

        return $this;
    }
}
