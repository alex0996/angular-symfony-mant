<?php

namespace App\Repository;

use App\Entity\Impuesto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Impuesto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Impuesto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Impuesto[]    findAll()
 * @method Impuesto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImpuestoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Impuesto::class);
    }



    public function transform(Impuesto $impuesto)
    {
        return [
                'id_impuesto'    => (int) $impuesto->getIdImpuesto(),
                'nombre_impuesto' => (string) $impuesto->getNombreImpuesto(),
                'monto' => (float) $impuesto->getMonto(),
                'fecha_desde' => (string) $impuesto->getFechaDesde(),
                'fecha_hasta' => (string) $impuesto->getFechaHasta(),
        ];
    }

    public function transformAll()
    {
        $impuestos = $this->findAll();
        $impuestosArray = [];

        foreach ($impuestos as $impuesto) {
            $impuestosArray[] = $this->transform($impuesto);
        }

        return $impuestosArray;
    }


    // /**
    //  * @return Impuesto[] Returns an array of Impuesto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Impuesto
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
